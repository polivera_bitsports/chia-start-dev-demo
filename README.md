# chia-start-dev-demo

## Introduction to Chialisp

https://www.chia.net/2019/11/27/chialisp.en.html

https://levelup.gitconnected.com/writing-my-first-chia-blockchain-program-part-1-introduction-a10cd8eddece

## Chialisp Documentation

# Docs

https://chialisp.com/docs

# CLVM reference

https://chialisp.com/docs/ref/clvm



## Getting started

1) Install python
https://www.python.org/downloads/


2) Create a directory where you will be storing files related to your Chia project. Such as /chia-password-coin.
3) Navigate into your newly created directory with the terminal.
4) Execute the following instructions 

```
python3 -m vend mychia
. /mychia/bin./activate
pip install chia-dev-tools
```

5) At this point you should be able to run the following commands from a terminal window that has your environment activated:

```
cdv -h
brun -h
run -h
```

6) Hello worl (Bitsports rocks)

```
brun '( q . "Bitsports rocks")'
```

# Documentation

https://github.com/Chia-Network/chia-dev-tools

https://github.com/Chia-Network/clvm_tools

https://github.com/Chia-Network/clvm_rs


## Docker

To get a docker, there is a link to download it:

https://github.com/Chia-Network/chia-docker

## Enable Visual Studio Code Extension

Everyone's use Visual Studio Code, so if you search on extensions, search by Chialisp and add it.

https://marketplace.visualstudio.com/items?itemName=clls-dev.clls-vscode

## Chia's structure coin
```
( mod
    (* Solution *)
    (* Main code *)
)
```

## Install the Chia Network

-     Download the Chia application `https://www.chia.net/download/`.
-     Install the downloadable executable.
-     Make sure you can access via the terminal by running the following command: chia -h

## Connet to Chia Testnet

The Chia's testnet money would be TXCH intead of XCH.

To configure our chia software in testnet, running the following commands:

```
chia init
chia configure -t true
```

## Chia Wallet

In the section before, we downloaded a executable that contains a wallet. Just run it.


## Chia Testnet

https://github.com/Chia-Network/chia-blockchain/wiki/How-to-connect-to-the-Testnet

https://github.com/Chia-Network/chia-blockchain/wiki/Create-a-local-testnet-for-fast,-private-testing


## Tokens (ERC20, ER223)

https://www.chia.net/2020/04/29/coloured-coins-launch.en.html

https://www.chia.net/2021/09/23/chia-token-standard-naming.en.html

https://github.com/Chia-Network/chia-blockchain/blob/main/chia/wallet/puzzles/cc.clvm

https://github.com/Chia-Network/chia-blockchain/blob/4a12db54a5b21136f9d55a735d2e44504fab0c61/chia/wallet/cc_wallet/cc_wallet.py

## Singleton (NFT)

https://chialisp.com/docs/puzzles/singletons

https://chialisp.com/docs/tutorials/singletons


## Structure of Chia Applications
https://chialisp.com/docs/tutorials/structure_of_a_chia_application


## Chia uses BLS 
https://en.wikipedia.org/wiki/BLS_digital_signature

## RPC and OpenAPI Specification

https://github.com/Chia-Network/chia-blockchain/wiki/RPC-Interfaces

https://github.com/irulast/chia-rpc-client-java/blob/master/chia-rpc.yaml 


